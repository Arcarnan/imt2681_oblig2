package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"bitbucket.org/Arcarnan/imt2681_oblig2/mongoDB"
)

/*
DISCLAIMER - the base for several of these functions are taken from the code of https://elithrar.github.io/article/testing-http-handlers-go/
*/

func TestTriggerWebhooks(t *testing.T) {
	request, err := http.NewRequest("GET", "/assignment2/evaluationtrigger/", nil)
	if err != nil {
		t.Fatal(err)
	}
	responseRecorder := httptest.NewRecorder()
	handler := http.HandlerFunc(handlerTrigger)
	handler.ServeHTTP(responseRecorder, request)
}

//testing of invoking the webhook when provided with the webhook ID
func TestInvokeWebhook(t *testing.T) {
	request, err := http.NewRequest("GET", "/assignment2/59fa5605a302142f91ca81ab", nil)
	if err != nil {
		t.Fatal(err)
	}
	responseRecorder := httptest.NewRecorder()
	handler := http.HandlerFunc(handlerWebhook)
	handler.ServeHTTP(responseRecorder, request)
}

func TestGetAverage(t *testing.T) {
	var data mongoDB.LatestAverageRequest

	data.BaseCurrency = "EUR"
	data.TargetCurrency = "NOR"
	result, err := json.Marshal(data)
	if err != nil {
		t.Fatal(err)
	}

	request, err := http.NewRequest("POST", "/assignment2/average/", ioutil.NopCloser(strings.NewReader(string(result))))
	request.Header.Add("Content-Type", "application/json")
	if err != nil {
		t.Fatal(err)
	}

	responseRecorder := httptest.NewRecorder()
	handler := http.HandlerFunc(handlerAverage)
	handler.ServeHTTP(responseRecorder, request)
}

func TestGetLatest(t *testing.T) {
	var data mongoDB.LatestAverageRequest

	data.BaseCurrency = "EUR"
	data.TargetCurrency = "NOR"
	result, err := json.Marshal(data)
	if err != nil {
		fmt.Print("error in latest marshalling")
		t.Fatal(err)
	}

	request, err := http.NewRequest("POST", "/assignment2/latest/", ioutil.NopCloser(strings.NewReader(string(result))))
	request.Header.Add("Content-Type", "application/json")
	if err != nil {
		fmt.Print("error in latest request")
		t.Fatal(err)
	}

	responseRecorder := httptest.NewRecorder()
	handler := http.HandlerFunc(handlerLastest)
	handler.ServeHTTP(responseRecorder, request)
}

//func TestAddWebhook(t *testing.T) {
//	var data mongoDB.RequestPayload
//
//	data.WebhookURL = "https://testwebhookurl"
//	data.BaseCurrency = "EUR"
//	data.TargetCurrency = "NOR"
//	data.MinTriggerValue = 1.0
//	data.MaxTriggerValue = 15.0
//
//	result, err := json.Marshal(data)
//	if err != nil {
//		fmt.Print("error in latest marshalling")
//		t.Fatal(err)
//	}
//
//	request, err := http.NewRequest("POST", "/", ioutil.NopCloser(strings.NewReader(string(result))))
//	request.Header.Add("Content-Type", "application/json")
//	if err != nil {
//		fmt.Print("error in latest request")
//		t.Fatal(err)
//	}
//
//	responseRecorder := httptest.NewRecorder()
//	handler := http.HandlerFunc(handlerWebhook)
//	handler.ServeHTTP(responseRecorder, request)
//}
