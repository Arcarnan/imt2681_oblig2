package main

/*
************	NOTE: due to the formulation of sending the ID, you have to end each search in the URL with "/"	************
 */

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"

	"bitbucket.org/Arcarnan/imt2681_oblig2/mongoDB"
	"gopkg.in/mgo.v2/bson"
)

var db = &mongoDB.CurrencyMongoDB{
	DatabaseURL:    "mongodb://arcarnan:arcarnan@ds115625.mlab.com:15625/imt2681_oblig2_database",
	DatabaseName:   "imt2681_oblig2_database",
	CollectionName: "currencyData"}

func handlerWebhook(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "POST":
		//	Registers a new webhook
		var payload mongoDB.RequestPayload

		//	Getting the payload data from user request
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			status := http.StatusBadRequest
			http.Error(w, http.StatusText(status), 400)
		}
		json.Unmarshal(body, &payload)

		webhookID, err := db.AddWebhook(payload)
		if err != nil || webhookID == "" {
			status := http.StatusBadRequest
			http.Error(w, http.StatusText(status), 400)
			return
		}

		w.WriteHeader(http.StatusCreated)
		log.Println("ID: ", webhookID)
		fmt.Fprintln(w, webhookID)

		result, err := json.Marshal(&payload)
		if err != nil {
			status := http.StatusBadRequest
			http.Error(w, http.StatusText(status), 400)
		}
		fmt.Fprintln(w, string(result))
		return

	case "GET":
		//	writes out the details of a webhook if the ID matches the ID of a registered webhook
		var getWebhook mongoDB.RequestPayload
		var responseWeb mongoDB.ResponseWebhook

		parts := strings.Split(r.URL.Path, "/")
		webhookID := parts[2]
		if bson.IsObjectIdHex(webhookID) == false { // webhookID == "" || len(webhookID) != 24 {
			//TODO this is where Mariusz's and Christofer's automated test fails
			status := http.StatusBadRequest
			http.Error(w, http.StatusText(status), 400)
			return
		}
		getWebhook, err := db.GetWebhook(webhookID)
		if err != nil {
			status := http.StatusNotFound
			http.Error(w, http.StatusText(status), 404)
			return
		}
		responseWeb.BaseCurrency = getWebhook.BaseCurrency
		responseWeb.TargetCurrency = getWebhook.TargetCurrency
		responseWeb.MinTriggerValue = getWebhook.MinTriggerValue
		responseWeb.MaxTriggerValue = getWebhook.MaxTriggerValue

		w.Header().Set("Content-Type", "application/JSON")
		raw, _ := json.Marshal(responseWeb)
		w.Write(raw)
		return

	case "DELETE":
		//	deletes a webhook from the database if the ID matches the ID of a registered webhook
		parts := strings.Split(r.URL.Path, "/")
		webhookID := parts[2]
		ok := db.DeleteWebhook(webhookID)
		if !ok {
			status := http.StatusBadRequest
			http.Error(w, http.StatusText(status), 400)
			return
		}
		status := http.StatusAccepted
		http.Error(w, http.StatusText(status), 410)
		return

	case "DEFAULT":
		http.Error(w, "not implemented yet - ", http.StatusNotImplemented)
		status := http.StatusNotImplemented
		http.Error(w, http.StatusText(status), 501)
		return
	}
}

//handlerAverage provides the average rates for a target currency for the last number of days
func handlerAverage(w http.ResponseWriter, r *http.Request) {
	days := 3

	var request mongoDB.LatestAverageRequest

	currencyCollection, err := db.FindAllRates()
	if err != nil {
		status := http.StatusBadRequest
		http.Error(w, http.StatusText(status), 400)
	}
	last := len(currencyCollection)

	//	Getting the request data from user
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		status := http.StatusBadRequest
		http.Error(w, http.StatusText(status), 400)
	}
	json.Unmarshal(body, &request)

	target := request.TargetCurrency
	base := request.BaseCurrency

	addedRate := 0.0
	for i := 1; i <= 3; i++ {
		currencyCollection[last-i].Rates["EUR"] = 1                                                                 //	the map does not include EUR as that is the default base, and since map is dynamic we can just insert it as a value of 1
		addedRate = addedRate + (currencyCollection[last-i].Rates[target] / currencyCollection[last-i].Rates[base]) //this function makes sure that any base and target currency is accepted
	}
	average := (addedRate / float64(days))
	fmt.Fprintln(w, average) //fmt.Fprintln(w, "Average for the last %v days: %v", days, average)
}

func handlerLastest(w http.ResponseWriter, r *http.Request) {
	var request mongoDB.LatestAverageRequest

	//	making sure the currency in the database is up to date, due to the fact that the heroku service sleeps the clock and web on the free version
	db.UpdateDatabase()

	//	Getting the request data from user
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		status := http.StatusBadRequest
		http.Error(w, http.StatusText(status), 400)
	}
	json.Unmarshal(body, &request)
	rate, err := db.GetLatest(request)
	if err != nil {
		status := http.StatusNotFound
		http.Error(w, http.StatusText(status), 404)
	}
	fmt.Fprintln(w, rate) //fmt.Fprintf(w, "\nLatest rate: %v", rate)
}

func handlerTrigger(w http.ResponseWriter, r *http.Request) {
	err := db.InvokeWebhooks()
	if err != nil {
		status := http.StatusNotFound
		http.Error(w, http.StatusText(status), 404)
	}
}

func main() {
	db.Init()
	http.HandleFunc("/assignment2/", handlerWebhook)
	http.HandleFunc("/assignment2/currency/", handlerWebhook)
	http.HandleFunc("/assignment2/latest/", handlerLastest)
	http.HandleFunc("/assignment2/evaluationtrigger/", handlerTrigger)
	http.HandleFunc("/assignment2/average/", handlerAverage)

	log.Println("http.ListenAndServe", http.ListenAndServe(":"+os.Getenv("PORT"), nil)) //	this is used to run online (heroku) - if .env file not working -> source .env (export PORT=5000)
}
