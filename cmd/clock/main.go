package main

import (
	"fmt"
	"log"
	"time"

	"bitbucket.org/Arcarnan/imt2681_oblig2/mongoDB"
)

var db = &mongoDB.CurrencyMongoDB{
	DatabaseURL:    "mongodb://arcarnan:arcarnan@ds115625.mlab.com:15625/imt2681_oblig2_database",
	DatabaseName:   "imt2681_oblig2_database",
	CollectionName: "currencyData"}

func main() {
	for {
		db.Init()
		err := db.InvokeWebhooks()
		if err != nil {
			log.Println(err)
			fmt.Printf("error in invoking webhooks: %v", err.Error())
		}
		db.UpdateDatabase()

		//delay := time.Second * 10
		delay := time.Minute * 20
		//delay := time.Hour * 24	//	apparently 24 hour sleep cycles renders the app unopperative
		time.Sleep(delay)
	}
}
