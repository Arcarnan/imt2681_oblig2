package mongoDB

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

/*
DISCLAIMER - the base for several of these functions are taken from the code of https://github.com/marni/imt2681_studentdb/blob/master/database.go
*/

//var Global_db StudentsStorage
const fixerAPICore = "http://api.fixer.io/latest"

//TODO user := os.Getenv("DB_USER")
//TODO password := os.Getenv("DB_PASSWORD")
var db = &CurrencyMongoDB{
	DatabaseURL:    "mongodb://arcarnan:arcarnan@ds115625.mlab.com:15625/imt2681_oblig2_database",
	DatabaseName:   "imt2681_oblig2_database",
	CollectionName: "currencyData"}

//CurrencyMongoDB stores the details of the DB connection.
type CurrencyMongoDB struct {
	DatabaseURL    string
	DatabaseName   string
	CollectionName string
}

//RequestPayload recieves the JSON body as request from user
type RequestPayload struct {
	ID              bson.ObjectId `bson:"_id,omitempty"`
	WebhookURL      string        `json:"webhookURL"`
	BaseCurrency    string        `json:"baseCurrency"`
	TargetCurrency  string        `json:"targetCurrency"`
	MinTriggerValue float64       `json:"minTriggerValue"`
	MaxTriggerValue float64       `json:"maxTriggerValue"`
}

//ResponseWebhook is the respone payload when invoking a webhook
type ResponseWebhook struct {
	BaseCurrency    string  `json:"baseCurrency"`
	TargetCurrency  string  `json:"targetCurrency"`
	CurrentRate     float64 `json:"currentRate,omitempty"`
	MinTriggerValue float64 `json:"minTriggerValue"`
	MaxTriggerValue float64 `json:"maxTriggerValue"`
}

//LatestAverageRequest saves the request for latest rates and for average from user
type LatestAverageRequest struct {
	BaseCurrency   string             `json:"baseCurrency"`
	TargetCurrency string             `json:"targetCurrency"`
	Rates          map[string]float64 `json:"rates"`
}

//StoredCurrencyData stores the currency data in memory
type StoredCurrencyData struct {
	BaseCurrency string             `json:"base"`
	Date         string             `json:"date"`
	Rates        map[string]float64 `json:"rates"`
}

//CurrencyFromFixer collects the data from the fixer API
type CurrencyFromFixer struct {
	Base  string             `json:"base"`
	Date  string             `json:"date"`
	Rates map[string]float64 `json:"rates"`
}

//UpdateDatabase updates the database with new data from fixer.oi
func (db *CurrencyMongoDB) UpdateDatabase() error {
	var CurrencyDataToStore CurrencyFromFixer

	session, err := db.DialMongoDBCurrency()
	if err != nil {
		return err
	}
	defer session.Close()
	JSONBody := GetRequestBody(fixerAPICore)
	json.Unmarshal(JSONBody, &CurrencyDataToStore)

	//	Adds the currency data to mongoDB database
	db.AddCurrency(CurrencyDataToStore)

	text := "Database updated at: " + time.Now().String()
	log.Println(text)
	return nil
}

//DialMongoDBCurrency dials the mongoDB database
func (db *CurrencyMongoDB) DialMongoDBCurrency() (*mgo.Session, error) {
	db.CollectionName = "currencyData"

	session, err := mgo.Dial(db.DatabaseURL)
	if err != nil {
		fmt.Print("cannot dial mongoDB in DialMongoDBCurrency - ")
		return nil, err
	}
	return session, nil
}

//DialMongoDBWebhooks dials the mongoDB database
func (db *CurrencyMongoDB) DialMongoDBWebhooks() (*mgo.Session, error) {
	db.CollectionName = "webhookCollection"

	session, err := mgo.Dial(db.DatabaseURL)
	if err != nil {
		fmt.Print("\ncannot dial mongoDB in DialMongoDBWebhooks - ")
		fmt.Print("\nDatabaseURL: - " + db.DatabaseURL)
		return nil, err
	}
	return session, nil
}

//Init initializes the mongoDB database storage
func (db *CurrencyMongoDB) Init() error {
	session, err := db.DialMongoDBCurrency()
	if err != nil {
		return err
	}
	defer session.Close()

	index := mgo.Index{
		Key:        []string{"date"},
		Unique:     true,
		DropDups:   true,
		Background: true,
		Sparse:     true,
	}

	err = session.DB(db.DatabaseName).C(db.CollectionName).EnsureIndex(index)
	if err != nil {
		return err
	}
	return nil
}

//AddCurrency adds the currency data for that day to the mongoDB database.
func (db *CurrencyMongoDB) AddCurrency(s CurrencyFromFixer) error {
	session, err := db.DialMongoDBCurrency()
	if err != nil {
		return err
	}
	defer session.Close()

	err = session.DB(db.DatabaseName).C(db.CollectionName).Insert(s)
	if err != nil {
		fmt.Printf("error in Insert(): %v", err.Error())
		return err
	}
	fmt.Print("ok. CurrencyData added") // 200 by default
	return nil
}

//AddWebhook adds the currency data for that day to the mongoDB database.
func (db *CurrencyMongoDB) AddWebhook(webhook RequestPayload) (string, error) {
	session, err := db.DialMongoDBWebhooks()
	if err != nil {
		log.Println("\nAddwebhook cannot dial mongoDB - ")
		return "", err
	}
	defer session.Close()

	if (webhook.WebhookURL != "") &&
		(len(webhook.BaseCurrency) == 3) &&
		(len(webhook.TargetCurrency) == 3) &&
		(webhook.MinTriggerValue > 0) &&
		(webhook.MaxTriggerValue > 0) {
		log.Println("\nAddwebhook DOES go into the if statement - ")

		// Generate an ID, Insert webhook
		webhook.ID = bson.NewObjectId()
		//text, _ := webhook.ID.MarshalText()	-	if key on "normal" form is needed
		err2 := session.DB(db.DatabaseName).C(db.CollectionName).Insert(webhook)
		if err2 != nil {
			fmt.Printf("error in Insert(): %v", err2.Error())
			return "", err2
		}

		fmt.Print("\nok. Webhook added") // 200 by default
		return webhook.ID.Hex(), nil
	}
	log.Println("\nAddwebhook does not go into the if statement - ")
	return "", err
}

//CountCurrency returns the current count of the data for currency in in-memory storage.
func (db *CurrencyMongoDB) CountCurrency() int {
	session, err := db.DialMongoDBCurrency()
	if err != nil {
		return 0
	}
	defer session.Close()

	// handle to "db"
	count, err := session.DB(db.DatabaseName).C(db.CollectionName).Count()
	if err != nil {
		fmt.Printf("error in Count(): %v", err.Error())
		return -1
	}
	return count
}

//CountWebhooks returns the current count of the data for currency in in-memory storage.
func (db *CurrencyMongoDB) CountWebhooks() int {
	session, err := db.DialMongoDBWebhooks()
	if err != nil {
		return 0
	}
	defer session.Close()

	count, err := session.DB(db.DatabaseName).C(db.CollectionName).Count()
	if err != nil {
		fmt.Printf("error in Count(): %v", err.Error())
		return -1
	}
	return count
}

//GetWebhook gets the webhook requested by id
func (db *CurrencyMongoDB) GetWebhook(webhookID string) (RequestPayload, error) {
	session, err := db.DialMongoDBWebhooks()
	if err != nil {
		return RequestPayload{}, err
	}
	defer session.Close()

	webhook := RequestPayload{}
	temp := bson.ObjectIdHex(webhookID)

	err = session.DB(db.DatabaseName).C(db.CollectionName).Find(bson.M{"_id": temp}).One(&webhook)
	if err != nil {
		log.Println(err)
		return RequestPayload{}, err
	}
	return webhook, nil
}

//InvokeWebhooks sends messages through webhooks created in the system
func (db *CurrencyMongoDB) InvokeWebhooks() error {
	var responseWeb ResponseWebhook

	webhooks, err := db.FindAllWebhooks()
	if err != nil {
		fmt.Print("\nFindAllWebhooks error in InvokeWebhooks")
		return err
	}

	webhookNumber := db.CountWebhooks()
	if webhookNumber >= 0 {

		for i := 0; i < webhookNumber; i++ {
			var request LatestAverageRequest

			responseWeb.BaseCurrency = webhooks[i].BaseCurrency
			responseWeb.TargetCurrency = webhooks[i].TargetCurrency
			responseWeb.MinTriggerValue = webhooks[i].MinTriggerValue
			responseWeb.MaxTriggerValue = webhooks[i].MaxTriggerValue

			request.BaseCurrency = responseWeb.BaseCurrency
			request.TargetCurrency = responseWeb.TargetCurrency

			rate, err := db.GetLatest(request)
			if err != nil {
				fmt.Println(err.Error())
				return err
			}

			if rate <= responseWeb.MinTriggerValue || rate >= responseWeb.MaxTriggerValue {
				responseWeb.CurrentRate = rate
				raw, _ := json.Marshal(responseWeb)
				resp, err := http.Post(webhooks[i].WebhookURL, "application/json", bytes.NewBuffer(raw))
				if err != nil {
					fmt.Println(err.Error())
					return err
				}
				_, err = ioutil.ReadAll(resp.Body)
				if err != nil {
					fmt.Println(err.Error())
					return err
				}
				//fmt.Println(s)
			}
		}
	}
	return nil
}

//FindAllWebhooks gets all the webhooks from the mongoDB database
func (db *CurrencyMongoDB) FindAllWebhooks() ([]RequestPayload, error) {
	session, err := db.DialMongoDBWebhooks()
	if err != nil {
		fmt.Println("Error: dial DialMongoDBWebhooks in FindAllWebhooks failed - ")
		return nil, err
	}
	defer session.Close()

	allWebhooks := []RequestPayload{}
	err = session.DB(db.DatabaseName).C(db.CollectionName).Find(nil).All(&allWebhooks)
	if err != nil {
		fmt.Println("Error: finding and storing all webhooks in FindAllWebhooks failed - ")
		return nil, err
	}
	return allWebhooks, nil
}

//FindAllRates gets all the currency data from the mongoDB database
func (db *CurrencyMongoDB) FindAllRates() ([]StoredCurrencyData, error) {
	session, err := db.DialMongoDBCurrency()
	if err != nil {
		return nil, err
	}
	defer session.Close()

	allRates := []StoredCurrencyData{}
	err = session.DB(db.DatabaseName).C(db.CollectionName).Find(nil).All(&allRates)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	return allRates, nil
}

//GetLatest gets the latest currency data from the mongoDB database
func (db *CurrencyMongoDB) GetLatest(request LatestAverageRequest) (float64, error) {
	rates, err := db.FindAllRates()
	if err != nil {
		return 0.0, err
	}
	last := len(rates)

	target := request.TargetCurrency
	base := request.BaseCurrency

	rates[last-1].Rates["EUR"] = 1                                        //	the map does not include EUR as that is the default base, and since map is dynamic we can just insert it as a value of 1
	rate := ((rates[last-1].Rates[target]) / (rates[last-1].Rates[base])) //this function makes sure that any base and target currency is accepted
	return rate, nil
}

//DeleteWebhook deletes a webhook from the moingoDB storage by _id
func (db *CurrencyMongoDB) DeleteWebhook(webhookID string) bool {
	session, err := db.DialMongoDBWebhooks()
	if err != nil {
		return false
	}
	defer session.Close()
	if webhookID != "" {

		fmt.Println("\nWebhook ID: " + webhookID)
		temp := bson.ObjectIdHex(webhookID)
		_, err = session.DB(db.DatabaseName).C(db.CollectionName).RemoveAll(bson.M{"_id": temp})
		if err != nil {
			log.Println(err)
			return false
		}
		return true
	}
	fmt.Print("\nInvalid webhook ID. " + webhookID)
	return false
}

//Datestring returns the date in a yyyy-mm-dd format
func Datestring(y, m, d int) (date string) {
	return fmt.Sprintf("%d-%02d-%02d", y, m, d)
}

//GetRequestBody returns the body from the API URL
func GetRequestBody(fixerAPI string) []byte {
	response, err := http.Get(fixerAPI)
	if err != nil {
		log.Println("\ngetRequestBody, response, error. ")
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Println("\ngetRequestBody, body, error. ")
	}
	response.Body.Close()
	return body
}
