package mongoDB

import (
	"testing"

	mgo "gopkg.in/mgo.v2"
)

//tests that one cannot dial an invalid database
var testFailDB = &CurrencyMongoDB{
	DatabaseURL:    "testFailURL",
	DatabaseName:   "testFailName",
	CollectionName: "testFailCollection"}

var testWebhook = RequestPayload{
	WebhookURL:      "http://webhook.site/64e4f7cc-0da7-49f9-aee7-8b1cc7f90abe",
	BaseCurrency:    "EUR",
	TargetCurrency:  "NOK",
	MinTriggerValue: 1.0,
	MaxTriggerValue: 10.0}

func TestInit(t *testing.T) {
	err := db.Init()
	if err != nil {
		t.Error(err)
	}

	err = testFailDB.Init()
	if err == nil {
		t.Error(err)
	}
	return
}

func TestUpdateDatabase(t *testing.T) {
	err := db.UpdateDatabase()
	if err != nil {
		t.Error("Cannot dial mongoDB")
	}

	err = testFailDB.UpdateDatabase()
	if err == nil {
		t.Error("Error - sucsessfull UpdateDatabase with bad URL - " + testFailDB.DatabaseURL)
	}
	return
}

func TestDialMongoDB(t *testing.T) {
	//Test dialing the currency collection
	_, err := db.DialMongoDBCurrency()
	if err != nil {
		t.Error("Testing dialMongoDB error - cannot dial mongoDB")
	}
	if db.DatabaseURL != "mongodb://arcarnan:arcarnan@ds115625.mlab.com:15625/imt2681_oblig2_database" {
		t.Fatalf("Error calling mongoDB database- imt2681_oblig2_database. Expected: %s - Got: %s", "currencyData", db.DatabaseURL)
	}
	_, err = mgo.Dial(db.DatabaseURL)
	if err != nil {
		t.Error("cannot dial mongo - ")
	}
	_, err = testFailDB.DialMongoDBCurrency()
	if err == nil {
		t.Error("Error - sucsessfull DialMongoDBCurrency with bad URL - " + testFailDB.DatabaseURL)
	}

	//Test dialing the webhooks collection
	_, err = db.DialMongoDBWebhooks()
	if err != nil {
		t.Error("Testing dialMongoDB error - cannot dial mongoDB")
	}
	if db.DatabaseURL != "mongodb://arcarnan:arcarnan@ds115625.mlab.com:15625/imt2681_oblig2_database" {
		t.Fatalf("Error calling mongoDB database- imt2681_oblig2_database. Expected: %s - Got: %s", "mongodb://arcarnan:arcarnan@ds115625.mlab.com:15625/imt2681_oblig2_database", db.DatabaseURL)
	}
	_, err = mgo.Dial(db.DatabaseURL)
	if err != nil {
		t.Error("cannot dial mongo - ")
	}

	_, err = testFailDB.DialMongoDBWebhooks()
	if err == nil {
		t.Error("Error - sucsessfull DialMongoDBWebhooks with bad URL - " + testFailDB.DatabaseURL)
	}
	return
}

func TestAddWebhookDatabasetest(t *testing.T) {
	var testWebhook = RequestPayload{
		WebhookURL:      "https://testwebhookurl",
		BaseCurrency:    "EUR",
		TargetCurrency:  "NOK",
		MinTriggerValue: 1.0,
		MaxTriggerValue: 10.0}

	addWebhookTestID, err := db.AddWebhook(testWebhook)
	if err != nil {
		t.Error("\nError in adding new webhook ", err)
		return
	}
	db.DeleteWebhook(addWebhookTestID)

	//_, err = testFailDB.AddWebhook(testWebhook)
	//if err == nil {
	//	t.Error("\nError: could not dial mongoDB ", err)
	//}
	//db.DeleteWebhook(addWebhookTestID)
}

func TestAddCurrency(t *testing.T) {
	var testCurrency = CurrencyFromFixer{
		Base: "EUR",
		Date: "2017-11-03",
	}

	err := db.AddCurrency(testCurrency)
	if err == nil {
		t.Error("\nError: managed to add currency data for existing date ", err)
	}

	err = testFailDB.AddCurrency(testCurrency)
	if err == nil {
		t.Error("\nError: could not dial mongoDB ", err)
	}
}

func TestCountCurrency(t *testing.T) {
	err := db.CountCurrency()
	if err < 0 {
		t.Error("\nError: could not dial mongoDB ", err)
	}
	err = testFailDB.CountCurrency()
	if err != 0 {
		t.Error("\nError: dialed a non-existing database ", err)
	}
}

func TestCountWebhooks(t *testing.T) {
	err := db.CountWebhooks()
	if err < 0 {
		t.Error("\nError: could not dial mongoDB ", err)
	}
	err = testFailDB.CountWebhooks()
	if err != 0 {
		t.Error("\nError: dialed a non-existing database ", err)
	}
}

func TestGetWebhook(t *testing.T) {
	addWebhookTestID, err := db.AddWebhook(testWebhook)
	if err != nil {
		t.Error("\nError in adding new webhook in TestGetWebhook - ", err)
		return
	}

	_, err = db.GetWebhook(addWebhookTestID)
	if err != nil {
		t.Error("\nError: did not manage to get webhook with valid ID. ", err)
	}

	//_, err = testFailDB.GetWebhook(addWebhookTestID)
	//if err == nil {
	//	t.Error("\nError: dialed a non-existing database ", err)
	//}
	db.DeleteWebhook(addWebhookTestID)
}

func TestInvokeWebhooks(t *testing.T) {
	err := testFailDB.InvokeWebhooks() //TODO crashes?
	if err == nil {
		t.Error("\nError: dialed a non-existing database ", err)
		return
	}

	err = db.InvokeWebhooks()
	if err != nil {
		t.Error("\nError: could not invoke webhooks. ", err)
	}
}

func TestFindAllWebhooks(t *testing.T) {
	//_, err := testFailDB.FindAllWebhooks()
	//if err == nil {
	//	t.Error("\nError: dialed a non-existing database ", err)
	//}

	_, err := db.FindAllWebhooks()
	if err != nil {
		t.Error("\nError: could not find webhooks in TestFindAllWebhooks - ", err)
	}
}

func TestFindAllRates(t *testing.T) {
	_, err := testFailDB.FindAllRates()
	if err == nil {
		t.Error("\nError: dialed a non-existing database ", err)
	}

	_, err = db.FindAllRates()
	if err != nil {
		t.Error("\nError: dialed a non-existing database ", err)
	}
}

func TestGetLatest(t *testing.T) {
	var testRequest = LatestAverageRequest{
		BaseCurrency:   "EUR",
		TargetCurrency: "NOK",
	}

	_, err := db.GetLatest(testRequest)
	if err != nil {
		t.Error("\nError: could not find latest rate ", err)
	}

	_, err = testFailDB.GetLatest(testRequest)
	if err == nil {
		t.Error("\nError: could not find latest rate ", err)
	}
}

func TestDeleteWebhook(t *testing.T) {
	var newTestWebhook = RequestPayload{
		WebhookURL:      "https://testwebhookurl",
		BaseCurrency:    "EUR",
		TargetCurrency:  "NOK",
		MinTriggerValue: 1.0,
		MaxTriggerValue: 20.0,
	}
	testWebhookID, err := db.AddWebhook(newTestWebhook)
	//fmt.Print("\nadded webhook int testDeleteWebhook: " + testWebhookID)
	if err != nil {
		t.Error("\nError: creating a test webhook in TestDeleteWebhook 1 ", err)
	}

	//deleted := testFailDB.DeleteWebhook(testWebhookID)
	//if deleted == false {
	//	t.Error("\nError: deleted webhook with incorrect dial TestDeleteWebhook - 1 "+testWebhookID, err)
	//}

	//testWebhookID, err = db.AddWebhook(newTestWebhook)//TODO delete this, testfaildb should not delete the webhook so we should still have the old add from the start of the fucntion
	//fmt.Print("\nadded webhook int testDeleteWebhook: " + testWebhookID)
	//if err != nil {
	//	t.Error("\nError: creating a test webhook in TestDeleteWebhook 2 ", err)
	//}

	deleted := db.DeleteWebhook(testWebhookID)
	if deleted == false {
		t.Error("\nError: could not delete webhook in TestDeleteWebhook - 2 "+testWebhookID, err)
	}
}

func TestGetRequestBody(t *testing.T) {
	_ = GetRequestBody(fixerAPICore)
}

func TestDatestring(t *testing.T) {
	expectedDate := "1999-02-22"
	if Datestring(1999, 2, 22) != expectedDate {
		t.Error("Date returned incorrectly from datestring")
	}
	return
}
