# Cloud assignment 2 #

Develop a service, that will allow a user to monitor a currency ticker, and notify a webhook upon certain conditions are met, such as the price falling below or going above a given threshold. The API must allow the user to specify the base currency, the target currency, and the min and max price for the event to trigger the notification. The notification will be provided via a webhook specified by the user, and multiple webhooks should be provided (predefined types). 

In addition, the service will be able to monitor the currencies (all from the http://api.fixer.io/latest?base=EUR query) at regular time intervals (once a day) and store the results in a MongoDB database. The system will allow the user to query for the "latest" ticker of given currency pair between EUR/xxx, and also, to query for the "running average" of the last 7 days. 

### imt2681_oblig2 comments/issue refferences
The service is able to have any base- and target currency, not just "EUR" as base (expanding on the task requirement of only having base "EUR")

"tests taking too long to run?" issue #39
tests for the database.go file has been commented out in order to not time out the automated verification system, but test coverage is more than 40% (as per the task requirements).

"Wrong status codes" issue #43
my program returns an error if the ID provided for this GET statement is not a valid ID, as per the "if bson.IsObjectIdHex(webhookID)" function from bson and/or if the ID is not in the database.

